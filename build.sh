#!/bin/bash
# Скрипт для сборки Docker образа

# Сборка проекта с использованием Maven
mvn clean package

# Построение Docker образа
docker build -t platesmania-scraper .