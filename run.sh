#!/bin/bash
# Скрипт для запуска Docker контейнера

# Создание директории для сохранения данных
mkdir -p data

# Запуск Docker контейнера
docker run --rm -v $(pwd)/data:/app/data platesmania-scraper